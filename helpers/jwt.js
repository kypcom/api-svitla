const  jwt = require('jsonwebtoken');

const generarJWT = (dta) => {

    const jwtSecret = process.env.JWT_SECRET || 'AARSHsvlta';

    const payload = {
        uid:dta.id,
        role:dta.role,
        personId:dta.personId
    };

    return new Promise((resolve, reject) => {

        jwt.sign(payload, jwtSecret, {

            expiresIn: '12h'
        }, (err, token) => {

            if (err) {
                console.log(err);
                reject('Error token')
            } else {
                resolve(token)
            }

        });

    });


}

module.exports =  generarJWT