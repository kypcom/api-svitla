const mongoose = require("mongoose");

const dbConeccion = async () => {

    try {

        await mongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            
        });

        console.log('Base de datos online');

    } catch (error) {

        console.log(error)

        throw new Error('Error en la base de datos');
    }


}


module.exports = dbConeccion;