
const Usuario = require('../models/Usuario');
const Transaccion = require('../models/Transaccion');
const Cuenta = require('../models/Cuenta');

const allTransaccion = async (req, res) => {


    const role = req.role;
    const personId = req.personId;
    const desde = Number(req.query.desde) || 0;

    console.log(personId)

    try {

        let filtro = {};

        if (role === 'USER_ROLE') {
            filtro = { personId: personId }
        }

        const transacciones = await Promise.all([
            Transaccion
                .find(filtro)
                .skip(desde)
                .limit(5),

            Transaccion.countDocuments()
        ]);

        return res.status(200).json({
            ok: true,
            transacciones,
            total
        });


    } catch (error) {
        console.log(error)
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
    }


}

const createTrans = async (req, res) => {

    try {
        const role = req.role;
        const personId = req.personId;

        const { cuentaOrigenId, numeroCuentaDestino, monto } = req.body;

        // Validar Saldo 
        const cuentOrigen = await Cuenta.findById(cuentaOrigenId);

        if (monto < cuentOrigen.saldo) {
            return res.status(403).json({
                msg: 'Saldo insuficiente'
            });
        }

        //Existe cuenta 
        const cuentaDestino = await Cuenta.findOne({ num_cuenta: numeroCuentaDestino });

        if (!cuentaDestino) {
            return res.status(404).json({
                msg: 'No existe la cuenta de destino'
            });
        }

        // Descontar saldo 

        const cuentaOrigenActualizada = await Cuenta.findByIdAndUpdate(cuentaOrigenId, { saldo: cuentOrigen.saldo - monto });

        //Crear transaccion 
        const transSalida = {
            cuenta_origen: cuentOrigen.id,
            cuenta_destino: cuentaDestino.id,
            monto,
            date: new Date(),
            persona: cuentOrigen.persona,
            tipo: 'RETIRO'
        }


        // Aumentar saldo 

        const cuentaDestinoActualizada = await Cuenta.findByIdAndUpdate(cuentaDestino._id, { saldo: cuentaDestino.saldo + monto });

        //Crear transaccion 
        const transEntrada = {
            cuenta_origen: cuentOrigen.id,
            cuenta_destino: cuentaDestino.id,
            monto,
            date: new Date(),
            persona: cuentaDestino.persona,
            tipo: 'DEPOSITO'
        }


        await new Transaccion(transSalida);
        await new Transaccion(transEntrada);


        return res.status(200).json({ msg: 'Transferencia realizada con eéxito' });



    } catch (error) {

        console.log(error)
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })

    }
}


module.exports = {
    allTransaccion,
    createTrans
};
