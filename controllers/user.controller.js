
const bcrypt = require('bcryptjs');
const Usuario = require('../models/Usuario');
const Persona = require('../models/Persona');


const createUser = async (req, res) => {

    const { user, nombre, apellidos, telefono, curp, direccion, password } = req.body;



    console.log(req.body);

    try {

        const existeUser = await Usuario.findOne({ user });

        if (existeUser) {
            return res.status(400).json({
                ok: false,
                msg: 'El usuario ya está registrado'
            });
        }

        const existePersona = await Persona.findOne({ curp });

        console.log(existePersona);

        if (existePersona) {
            return res.status(400).json({
                ok: false,
                msg: 'La persona ya está registrado'
            });
        }

        const personReq = {
            direccion,
            nombre,
            apellidos,
            curp,
            telefono
        }

        //Create persona 
        const persona = new Persona(personReq);
        await persona.save();

        const userReq = {
            user,
            password,
            persona: persona.id,

        }

        // crete User 
        const usuario = new Usuario(userReq);

        // Encriptar contraseña
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync(password, salt);

        // Guardar usuario
        await usuario.save();

        // Generar el TOKEN - JWT
        return res.status(200).json({
            ok: true,
            usuario
        });


    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }


}


const updateUser = async (req, res) => {

    try {

        const uid = req.uid;
        //const persona = await Persona.findById( personId );

        const usuarioActualizada = await Usuario.findByIdAndUpdate( uid, {estatus:false}, { new: true } );


        return res.status(200).json({msg:'Usuari Cancelado'});



    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }

}


const deleteUser = async (req, res) => {
    try {

        const personId = req.personId;
        //const persona = await Persona.findById( personId );


        const campos={
            direccion:req.body.direccion,
            telefono:req.body.telefono
        }

        const personaActualizada = await Persona.findByIdAndUpdate( personId, campos, { new: true } );


        return res.status(200).json(personaActualizada);



    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
}

module.exports = {
    createUser,
    updateUser,
    deleteUser
};
