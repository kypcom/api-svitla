
const bcrypt = require('bcryptjs');

const Usuario = require('../models/Usuario');
const Session = require('../models/Session');
const generarJWT = require('../helpers/jwt');

const Login = async (req, res) => {

    const { user, password } = req.body;

    try {

        // Verificar email
        const usuarioDB = await Usuario.findOne({ user });

        if (!usuarioDB) {
            return res.status(404).json({
                ok: false,
                msg: 'Usuario no encontrado'
            });
        }


        if (!usuarioDB.estatus) {
            return res.status(403).json({
                ok: false,
                msg: 'Usuario no activo'
            });
        }

        // Buscar sESSION Activa 


        // Verificar contraseña
        const validPassword = bcrypt.compareSync(password.toString(), usuarioDB.password);
        if (!validPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'Contraseña no válida'
            });
        }

        // Find Sessions Aactives 


        // Generar el TOKEN - JWT

        console.log(usuarioDB.persona.id);

        const payload = {
            id: usuarioDB.id,
            role: usuarioDB.role,
            personId: usuarioDB.persona.id
        }

        const token = await generarJWT(payload);


        // Generar session


        const sessionData = {
            user: usuarioDB.id,
            token
        }

        new Session(sessionData);


        return res.json({
            ok: true,
            token
        })

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
    }


}

module.exports = {
    Login
}

