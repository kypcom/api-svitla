"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.crearUsuario = void 0;
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const Usuario_1 = __importDefault(require("../models/Usuario"));
const Persona_1 = __importDefault(require("../models/Persona"));
const crearUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { user, nombre, apellidos, telefono, curp, direccion, password } = req.body;
    try {
        const existeUser = yield Usuario_1.default.findOne({ user });
        if (existeUser) {
            return res.status(400).json({
                ok: false,
                msg: 'El usuario ya está registrado'
            });
        }
        const existePersona = yield Usuario_1.default.findOne({ curp });
        if (existePersona) {
            return res.status(400).json({
                ok: false,
                msg: 'La persona ya está registrado'
            });
        }
        //Create persona 
        const persona = new Persona_1.default(req.boby);
        yield persona.save();
        // crete User 
        const usuario = new Usuario_1.default(req.body);
        // Encriptar contraseña
        const salt = bcryptjs_1.default.genSaltSync();
        usuario.password = bcryptjs_1.default.hashSync(password, salt);
        usuario.persona = persona.id;
        // Guardar usuario
        yield usuario.save();
        // Generar el TOKEN - JWT
        res.json({
            ok: true,
            usuario
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }
});
exports.crearUsuario = crearUsuario;
//# sourceMappingURL=user.controller.js.map