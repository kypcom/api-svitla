"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const config_1 = require("../database/config");
//Routes 
const auth_route_1 = __importDefault(require("../routes/auth.route"));
const user_route_1 = __importDefault(require("../routes/user.route"));
class Server {
    constructor() {
        this.apiPaths = {
            auth: '/api/auth',
            user: '/api/user',
            transaction: '/api/transaccion'
        };
        this.app = express_1.default();
        this.port = process.env.PORT || '5000';
        //definir rutas 
        config_1.dbConnection();
        this.middlewares();
        this.routes();
        this.app.use(express_1.default.static('public'));
    }
    middlewares() {
        this.app.use(cors_1.default());
        this.app.use(express_1.default.json());
    }
    routes() {
        this.app.use(this.apiPaths.auth, auth_route_1.default);
        this.app.use(this.apiPaths.user, user_route_1.default);
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor arriba en puerto : ' + this.port);
        });
    }
}
exports.default = Server;
//# sourceMappingURL=Server.js.map