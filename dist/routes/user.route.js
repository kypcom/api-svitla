"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const validate_jwt_1 = __importDefault(require("../middlewares/validate-jwt"));
const usuario_controller_1 = require("../controllers/usuario.controller");
const router = express_1.Router();
router.post('/create', [validate_jwt_1.default], usuario_controller_1.createUser);
router.post('/update', [validate_jwt_1.default], usuario_controller_1.updateUser);
router.post('/baja', [validate_jwt_1.default], usuario_controller_1.deleteUser);
exports.default = router;
//# sourceMappingURL=user.route.js.map