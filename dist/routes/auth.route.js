"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const validate_inputs_1 = __importDefault(require("../middlewares/validate-inputs"));
const auth_controller_1 = require("../controllers/auth.controller");
const router = express_1.Router();
router.post('/login', [
    express_validator_1.check('user', 'Usert required').isEmail(),
    express_validator_1.check('password', 'Password required').not().isEmpty(),
    validate_inputs_1.default
], auth_controller_1.Login);
exports.default = router;
//# sourceMappingURL=auth.route.js.map