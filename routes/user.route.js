const { Router } = require('express');


const  validarJWT  = require('../middlewares/validate-jwt');
const { createUser, updateUser, deleteUser } = require( '../controllers/user.controller');


const router = Router();

router.post('/create', createUser);
//router.post('/update', validarJWT, updateUser);
//router.post('/baja', validarJWT, deleteUser);

module.exports =  router;