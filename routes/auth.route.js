const { Router } = require('express');
const { check } = require('express-validator');

const validarCampos = require('../middlewares/validate-inputs');

const { Login } = require('../controllers/auth.controller');

const router = Router();

router.post('/login', [
    check('user', 'User required').not().isEmpty(),
    check('password', 'Password required').not().isEmpty(),
    validarCampos
], Login);


module.exports = router;