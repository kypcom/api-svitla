const { Router } = require('express');


const  validarJWT  = require('../middlewares/validate-jwt');

const { allTransaccion, createTrans } = require( '../controllers/transaccion.controller');


const router = Router();

router.get('/all', [validarJWT], allTransaccion);
router.post('/create', [validarJWT], createTrans);



module.exports =  router;