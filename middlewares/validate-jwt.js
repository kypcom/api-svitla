const  jwt = require('jsonwebtoken') ;


const validarJWT = (req , res , next) => {

    // Leer el Token
    const token = req.header('x-token');

    if (!token) {
        return res.status(401).json({
            ok: false,
            msg: 'No token in request'
        });
    }

    try {

        console.log(token)

        const { uid , role , personId } = jwt.verify(token, process.env.JWT_SECRET || 'AARSHsvlta');
        req.uid = uid;
        req.role = role;
        req.personId = personId;

        next();

    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Token invalid'
        });
    }

}

module.exports =  validarJWT;