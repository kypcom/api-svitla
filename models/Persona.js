const { Schema, model } = require('mongoose');

const PersonaSchema = new Schema({

    nombre: {
        type: String,
        required: true
    },
    apellidos: {
        type: String,
        required: true,
    },
    curp: {
        type: String,
        required: true,
        unique: true
    },
    telefono: {
        type: Number,
        required: true,
    },
    direccion: {
        type: String,
        required: true,
    }

});


PersonaSchema.method('toJSON', function () {
    const { __v,_id, ...object } = this.toObject();
    object.pid = _id;
    return object;
});

module.exports = model('Persona', PersonaSchema);
