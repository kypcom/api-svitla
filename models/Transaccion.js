const { Schema, model } = require('mongoose');

const TransaccionSchema = Schema({
    cuenta_origen: {
        type: Schema.Types.ObjectId,
        ref: 'Cuenta',
        required: true
    },
    cuenta_destino: {
        type: Schema.Types.ObjectId,
        ref: 'Cuenta',
        required: true
    },
    monto: {
        type: Number,
        require: true,
        default: 0

    },
    tipo: {
        type: String,
        require: true,
    },
    date: {
        type: Date,
        require: true
    },
    persona: {
        type: Schema.Types.ObjectId,
        ref: 'Persona',
        required: true
    },
    estatus: {
        type: Boolean,
        required: true,
        default: false
    },
});


TransaccionSchema.method('toJSON', function () {
    const { __v, _id, ...object } = this.toObject();
    object.uid = _id;
    return object;
})



module.exports = model('Transaccion', TransaccionSchema);
