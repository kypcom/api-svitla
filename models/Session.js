const { Schema, model } = require('mongoose');

const SessionSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    },
    token: {
        type: String,
        required: true,
        unique: true
    },
    active: {
        type: Boolean,
        required: true,
        default:true
        
    }

});


SessionSchema.method('toJSON', function () {
    const { __v, ...object } = this.toObject();
    return object;
})



module.exports =  model('Session', SessionSchema);
