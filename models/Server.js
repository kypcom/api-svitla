const express = require('express');
const cors = require('cors');
const dbConeccion = require('../database/config');

//Routes 

const  authRoute =require('../routes/auth.route') ;
const  userRoute = require('../routes/user.route');
const  transRoute = require('../routes/transaction.route');

class Server {

   
    
     apiPaths = {
        auth: '/api/auth',
        user: '/api/user',
        transaction: '/api/transaccion'

    };

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '5000';

        //definir rutas 
        this.conectarDB();
        this.middlewares();
        this.routes();
        this.app.use(express.static('public'));


    }

    async conectarDB() {
        await dbConeccion()
    }


    middlewares() {
        this.app.use(cors('*'));
        this.app.use(express.json());
    }

    routes() {
        this.app.use(this.apiPaths.auth, authRoute);
        this.app.use(this.apiPaths.user, userRoute);
        this.app.use(this.apiPaths.transaction, transRoute);
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor arriba en puerto : ' + this.port);
        })
    }
}

module.exports = Server;