const { Schema, model } = require('mongoose');

const CuentaSchema =  Schema({
    num_cuenta: {
        type: Number,
        required: true,
        unique:true
    },
    producto: {
        type: String,
       
    },
    saldo: {
        type: Number,
        required: true,
        default: 0
    },
    persona: {
        type: Schema.Types.ObjectId,
        ref: 'Persona',
        required: true
    },
    nip: {
        type: Number,
        required: true,
    },
    estatus: {
        type: Boolean,
        required: true,
        default:false
    },
});


CuentaSchema.method('toJSON', function () {
    const { __v, _id, ...object } = this.toObject();
    object.uid = _id;
    return object;
})



module.exports = model('Cuenta', CuentaSchema);
